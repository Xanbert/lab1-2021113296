#include "utils.h"
#include <fcntl.h>
#include <filesystem>
#include <format>
#include <fstream>
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>
#include <unistd.h>

using namespace std::filesystem;

uint walk_times[10] = { 0, 1, 1, 2, 2, 3 };

int
testcase (int x, path &&p)
{
  path p_text (p);
  path p_input (p);
  std::ifstream text (p_text.append (std::format ("{}.txt", x)));
  if (!text.is_open ())
    throw "Text not opened";
  utils::readMap (text);
  text.close ();

  std::stringstream output;

  for (int i = 0; i < walk_times[x]; i++)
    {
      auto out = utils::randomWalk ();
      if (out != "")
        output << out << " ";
      else
        break;
      ;
    }

  std::string dest = [x] (path p) {
    std::ifstream fs (p.append (std::format ("{}.out", x)));
    std::stringstream buffer;
    buffer << fs.rdbuf ();
    return buffer.str ();
  }(p);
  if (dest.compare (output.str ()) != 0)
    {
      std::cerr << "Output: " << output.str () << std::endl;
      std::cerr << "Dest: " << dest << std::endl;
    }
  return 0;
}

int
main (int argc, char **argv)
{
  if (argc != 3)
    return -1;
  return testcase (std::stoi (argv[1]), std::move (path (argv[2])));
}
