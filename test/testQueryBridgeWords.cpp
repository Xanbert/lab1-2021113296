#include "utils.h"
#include <fcntl.h>
#include <filesystem>
#include <format>
#include <fstream>
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>
#include <unistd.h>

using namespace std::filesystem;

int
testcase (int x, path &&p)
{
  path p_text (p);
  path p_input (p);
  std::ifstream text (p_text.append (std::format ("{}.txt", x)));
  if (!text.is_open ())
    throw "Text not opened";
  utils::readMap (text);
  text.close ();
  std::ifstream input (p_input.append (std::format ("{}.in", x)));
  if (!input.is_open ())
    throw "Input not opened";
  std::string word1, word2;
  std::getline (input, word1);
  std::getline (input, word2);
  input.close ();
  auto output = utils::queryBridgeWords (word1, word2);
  std::string dest = [x] (path p) {
    std::ifstream fs (p.append (std::format ("{}.out", x)));
    std::stringstream buffer;
    buffer << fs.rdbuf ();
    return buffer.str ();
  }(p);
  if (dest.compare (output) != 0)
    {
      std::cerr << "Output: " << output << std::endl;
      std::cerr << "Dest: " << dest << std::endl;
    }
  return 0;
}

int
main (int argc, char **argv)
{
  if (argc != 3)
    return -1;
  return testcase (std::stoi (argv[1]), std::move (path (argv[2])));
}
